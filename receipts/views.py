from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CatogoryForm, AccountForm


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipts,
    }

    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        create_form = ReceiptForm(request.POST)
        if create_form.is_valid():
            receipt = create_form.save(False)
            receipt.purchaser = request.user
            receipt.save()
        return redirect("home")
    else:
        create_form = ReceiptForm()

    context = {
        "create_form": create_form
    }

    return render(request, "receipts/create.html", context)


@login_required
def expense_category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)

    context = {
        "categories": categories
    }

    return render(request, "receipts/categories_list.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)

    context = {
        "accounts": accounts
    }

    return render(request, "receipts/account_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
      create_category_form = CatogoryForm(request.POST)
      if create_category_form.is_valid():
          category = create_category_form.save(False)
          category.owner = request.user
          category.save()
          return redirect("category_list")
    else:
        create_category_form = CatogoryForm()

    context = {
        "create_category_form": create_category_form
    }

    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        create_account_form = AccountForm(request.POST)
        if create_account_form.is_valid():
            account = create_account_form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        create_account_form = AccountForm()

    context = {
        "create_account_form": create_account_form
    }

    return render(request, "receipts/create_account.html", context)
